import sys,math
from pybioshell.core.data.io import Pdb

class MSA:
  def __init__(self,msa_filename,pdb_filename,sub_file,query_name=""):
    
    self.msa_file = msa_filename
    self.pdb_file = pdb_filename
    self.sup_file = sup_file
    self.msa_seqs = self.read_malf()
    reader = Pdb(pdb_file,"is_not_water",False)
    self.structure = reader.create_structure(0)
    self.pdb_list = self.get_chain_list()
    print(self.pdb_list)
    #self.pdb_list = self.fix_chain_list(self.structure)
    if query_name == "":
      self.query = self.pdb_list[0]
    else:
      self.query = query_name
    self.conserved_positions = self.return_conserved_positions()
    self.conserved_surpass = []


  def read_malf(self): 
    """Zwraca slownik: (nazwa_bialka : sekwencja)"""
    proteins ={}
    with open(self.msa_file) as f:
      data  = f.readlines()
      for line in data[2:]: # skip first two lines
        tokens = line.strip().split()
        if len(tokens) != 2: continue #skip lines which doesn't have 2 columns
        if tokens[0] == "Percentage":
          break
        if tokens[0] in proteins:
          proteins[tokens[0]]+=tokens[1]
        else:
          if tokens[0][0] != "0":
            proteins[tokens[0]]=tokens[1]
    return proteins

  def return_conserved_positions(self, allowed_gaps=0): 
    """Przyjmuje slownik MSA, zwraca liste konserwowanych pozycji
    Konserwowana pozycja to taka, ktora ma co najwyzej allowed_gaps 
    spacji - domyslnie 0 czyli musza byc aminokwasy od gory do dolu
    """
    conserved_pos = []
    for position in range(len(list(self.msa_seqs.values())[0])-1):
      space_counter = 0
      for prot in self.msa_seqs:
        if self.msa_seqs[prot][position] == "-":
          space_counter +=1
        if space_counter > allowed_gaps:
          break
      if space_counter <= allowed_gaps:
        #for prot in msa_dict:
          #conserved_pos[prot].append( give_protein_position(msa_dict[prot],position) )
        conserved_pos.append( position )


    
    print(len(conserved_pos))
    return conserved_pos

  def get_chain_list(self):
    """Sprawdza czy w sup.pdb jest lista bialko-lancuch, jesli nie to bierze ja z pliku .malf"""
    chain_list = []
    with open(self.sup_file) as f:
      data = f.readlines()
      if data[5].strip() == "REMARK The domains in this file are:" :
        for line in data[6:]:
          tokens = line.strip().split()
          if len(tokens) > 4: break
          if tokens[2] != "chain" : break
          chain_list.append(tokens[1])
        return chain_list
    with open(self.msa_file) as fm:
      data = fm.readlines()
      for line in data[2:]:
        tokens = line.strip().split()
        if len(tokens) == 0 : break
        chain_list.append(tokens[2])
      return chain_list

  def fix_chain_list(self,structure):
    """Fixes wrong chain_list"""
    new_list = []
    for i in range(structure.count_chains()):
      chain = structure[i]
      seq = chain.create_sequence().sequence
      for pdb,sequence in self.msa_seqs.items():
        dlugosc = len(self.get_clear_sequence(sequence))
        if seq[:dlugosc]==self.get_clear_sequence(sequence):
          new_list.append(pdb)
    print("fixed",new_list)
    return new_list

  def calculate_distances(self):
    matrix_list = []
    for pdb in self.pdb_list:
      if pdb != self.query :
      #matrix_list.append(compute_distances(positions[pdb],structure[new_list.index(pdb)],msa[pdb]))
        matrix_list.append(self.compute_distances(pdb,self.structure[self.pdb_list.index(pdb)]))
    return matrix_list

  def compute_distances(self,pdb,chain):
    """Wczytuje PDB, idzie po kolei po resztach i dla tych na zadanych 
    pozycjach oblicza parami odleglosci; zwraca macierz 2D. 
    Zakladamy ze wczytywany jest zawsze jeden lancuch"""
    query_len = len(self.get_clear_sequence(self.msa_seqs[self.query]))
    
    distance_matix =  [[10000 for i in range(query_len)] for j in range(query_len)]

    if len(chain.create_sequence().sequence)+3 != len(self.get_clear_sequence(self.msa_seqs[pdb])):
      print(self.pdb_file," Sequences are not the same!")
      print(chain.create_sequence().sequence)
      print(self.get_clear_sequence(self.msa_seqs[pdb]))
      exit()
      
    
    #if chain.count_residues() <= self.conserved_positions[-1] : 
    #  print("Error! Last position is greather then number of count_residues()!")
    #  return -1
    for pos1 in self.conserved_surpass:
      if pos1 == -1 : continue
      for pos2 in self.conserved_surpass:
        if pos2 == -1 : continue
        d = chain[self.get_protein_position(self.msa_seqs[pdb],pos1)][0].distance_to(chain[self.get_protein_position(self.msa_seqs[pdb],pos2)][0])
        distance_matix[self.get_protein_position(self.msa_seqs[self.query],pos1)][self.get_protein_position(self.msa_seqs[self.query],pos2)] = d
    #print(distance_matix)
    return distance_matix


  def get_clear_sequence(self,seq):
    """Przyjmuje sekwencje z przerwami. Zwraca bez przerw"""
    clear_seq=""
    for i in range(len(seq)):
      if seq[i] != "-" and seq[i] != "*":
        clear_seq+=seq[i]
    return clear_seq


  def get_protein_position(self,sequence,position):
    """Liczy ilosc przerw do podanej position i zwraca prawdziwy indeks dla sekwencji"""
    space_counter = 0
    for i in range(position):
      if sequence[i] == "-":
        space_counter += 1

    if sequence[position] == "-": return -1
    return position - space_counter

  def find_surpass_conserved(self):
    print(self.conserved_positions)
    for i in range(len(self.conserved_positions)-3):
      if self.conserved_positions[i+3]-self.conserved_positions[i] == 3:
        self.conserved_surpass.append(self.conserved_positions[i])
    print(self.conserved_surpass)



def prepare_constraints(matrix_list, max_distance=10.0, max_std=5):
  """Pobiera liste macierzy2D  zwroconych przez compute_distances;
  dla kazdej pary indeksow i,j idzie po wszystkich macierzach i wyznacza 
  srednia i odchylenie. Jak srednia jest mniejsza od max_distance
  a odchylenie mniejsze nia max_std, to wlada na liste (i,j,avg, stdev); 
  zwraca te liste"""
  constraints = []
  #print(matrix_list)
  N = len(matrix_list[0][0])
  print(N)
  for i in range(N):
    for j in range(i+3,N):
      suma_d = 0
      suma_kw = 0
      for prot in range(len(matrix_list)):
        #if i==419 and j==421:
        #  print(matrix_list[prot][i][j])
        if matrix_list[prot][i][j]!=10000:
          suma_d += matrix_list[prot][i][j]
          suma_kw += matrix_list[prot][i][j]*matrix_list[prot][i][j]

      srednia = suma_d/len(matrix_list)
      #print(srednia,suma_kw)
      stdev2 = suma_kw/len(matrix_list) - (srednia*srednia)
      #print(stdev2)
      if srednia != 0 and srednia < max_distance and 0 <= stdev2 < max_std*max_std:
        #print(i,j,srednia,stdev2)
        constraints.append([i,j,srednia,math.sqrt(stdev2)])
  return constraints





if __name__ == "__main__":
  if len(sys.argv) < 2:
    print("Usage: python3.7 read_msa_and_calc_dist.py family_name [query_protein]")
    exit()
  
  malf = "/home/users/mpiwowar/STORAGE/DATABASES/HOMSTRAD-May20/SEQUENCES/"+sys.argv[1]+"/"+sys.argv[1]+".malf"
  sup_file = "/home/users/mpiwowar/STORAGE/DATABASES/HOMSTRAD-May20/SEQUENCES/"+sys.argv[1]+"/"+sys.argv[1]+"-sup.pdb"
  #pdb_file = "/home/users/jkrys/src.git/surpass_cm/INPUTS/"+sys.argv[1]+"-surpass.pdb"
  pdb_file = sys.argv[1]+"-surpass.pdb"
  if len(sys.argv)>2:
    query = sys.argv[2]
    msa = MSA(malf,pdb_file,sup_file,query)
  else:
    msa = MSA(malf,pdb_file,sup_file)

  # msa = read_malf(malf)
  # positions = conserved_positions(msa)
  # reader = Pdb(pdb_file,"is_ca",False)
  # structure = reader.create_structure(0)
  # pdb_list = get_chain_list(pdb_file,malf)
  # print(pdb_list)
  # new_list = fix_chain_list(pdb_list,msa,structure)

  msa.find_surpass_conserved()  
  # matrix_list = []
  # print(new_list)
  # for pdb in new_list:
  #   if pdb != query :
  #     matrix_list.append(compute_distances(positions[pdb],structure[new_list.index(pdb)],msa[pdb]))
  matrix_list = msa.calculate_distances()
  #con = prepare_constraints(matrix_list)
  #with open("/home/users/jkrys/src.git/surpass_cm/INPUTS/"+sys.argv[1]+"-surpass.cn.new","w+") as file:
#    for a in con:
#      file.write("%2d  %2d  %5.3f  %4.3f\n" % (a[0],a[1],a[2],a[3]) )

 # print("Found ",len(con)," constraints!")


