
import sys
from pybioshell.core.data.io import Pdb, write_pdb
from pybioshell.core.data.structural import Structure, Chain
from pybioshell.simulations.representations import surpass_representation
if len(sys.argv) < 2 :
  print("""

Converts an all-atom PDB file to SURPASS representation

USAGE:
    python3 to_surpass_system.py input.pdb [output.pdb]
""")
  exit()
cn = 0
reader = Pdb(sys.argv[1], "is_ca", True)
strctr = reader.create_structure(0)
res_in_chains=[]
chains = []
for i in range(strctr.count_chains()):
  chain = strctr[i]
  chains.append(chain.id())
  res_in_chains.append([0 for r in range(chain.count_residues()*2)])
  print(chain[0].id(),chain[chain.count_residues()-1].id(),chain.count_residues())
  for j in range(chain.count_residues()):
    #print(chain[j].id())
    res_in_chains[i][chain[j].id()-chain[0].id()]=chain[j]
    #if chain[j].id()+1!=chain[j+1].id():
     # cn+=1
      #print(sys.argv[1].split("/")[-2]," ",chain.id())
      #break
  #break
  
new_str = Structure(sys.argv[1].split("/")[-2])
for chain in range(len(res_in_chains)):
  ch = Chain(chains[chain])
  for res in res_in_chains[chain]:
    if res!=0:
      ch.push_back(res)
  new_str.push_back(ch)

#for i in range(new_str[2][56].count_atoms()):
#  if new_str[2][56][i].atom_name()==" CA ":
#    a=new_str[2][56][i]
#for i in range(new_str[2][57].count_atoms()):
#  if new_str[2][57][i].atom_name()==" CA ":
#    b=new_str[2][57][i]
#d = a.distance_to(b)
#print("dist ",d)

surpas = surpass_representation(new_str)
print(strctr.count_residues(), new_str.count_residues(), new_str.count_residues()-surpas.count_residues(), surpas.count_chains()*3)
#out_name = sys.argv[2] if len(sys.argv) > 2 else "out.pdb"
out_name = sys.argv[1].split("/")[-2]+"-surpass.pdb"
write_pdb(surpas, out_name)