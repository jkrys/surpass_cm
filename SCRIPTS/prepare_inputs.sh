FAMILY=$1
PDB=$2
CHAIN=$3
BEGIN=$4
END=$5

#curl -O https://files.rcsb.org/view/$PDB.pdb
strc -in:pdb=$PDB.pdb -select::substructure="$CHAIN":"$BEGIN"-"$END" -op=native.pdb
#seqc -in:pdb=$PDB.pdb -out:ss2=$FAMILY.ss2 -in:pdb:header
ex_SurpassModel native.pdb